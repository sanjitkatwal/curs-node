const http = require("http");

const server = http.createServer((req, res) => {
  if (req.method == "GET") {
    switch (req.url) {
      case "/":
        res.end("You are at home page.");
        break;
      case "/customer":
        res.end("You are at customer page.");
        break;

      default:
        res.writeHead(404, "page not found", { "Content-Type": "text/plain" });
        res.end("Page not found.");
    }
  } else if (req.method == "POST") {
  } else {
    res.writeHead(405, { "Content-Type": "text/plain" });
    res.end("Method is not allowed");
  }
});

server.listen(3001, () => {
  console.log("server is running at port 3001");
});
